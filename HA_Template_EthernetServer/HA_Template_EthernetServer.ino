#include <Ethernet.h>

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};

IPAddress ip(192, 168, 0, 50);

int relayOnePin = 2;
int relayTwoPin = 3;

bool newP1state = false;
bool newP2state = false;

int p1;
int p2;

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);

void setup() {
	// Open serial communications and wait for port to open:
	Serial.begin(9600);
	
	pinMode(relayOnePin, OUTPUT);
	pinMode(relayTwoPin, OUTPUT);

	// start the Ethernet connection and the server:
	Ethernet.begin(mac, ip);
	server.begin();

	Serial.print("Server is at ");
	Serial.println(Ethernet.localIP());
}

void okResponse(EthernetClient client) {
	client.println("HTTP/1.1 200 OK");
	client.println("Content-Type: application/json");
	client.println();
	client.println("{\"status\":\"Ok\"}");
}

void badRequestResponse(EthernetClient client) {
	client.println("HTTP/1.1 400 Bad Request");
	client.println("Content-Type: application/json");
	client.println();
	client.println("{\"status\":\"Bad request\"}");
}

void jsonResponse(EthernetClient client, String paramName, int val) {
	String json = "{\"param\":\"";
	json += paramName;
	json += "\",\"value\":\"";
	json += val;
	json += "\"}";

	client.println("HTTP/1.1 200 OK");
	client.println("Content-Type: application/json");
	client.println("Access-Control-Allow-Origin: *");
	client.println();
	client.println(json);
}

void actionOne(int val) {
	// do something
}

void actionTwo(int val) {
	// do something
}

void processPrevRequest() {
	if (newP1state) {
		actionOne(p1);
		newP1state = false;
	}
	else if (newP2state) {
		actionTwo(p2);
		newP2state = false;
	}
}

void processNextRequest() {
	// Check if a client has connected
	EthernetClient client = server.available();

	if (!client) {
		return;
	}

	// Wait until the client sends some data
	//Serial.println("New client!");
	while (client.connected()) {
		if (client.available()) {
			String req = client.readStringUntil('\r');
			//Serial.println(req);

			client.flush();

			if (req.indexOf("/p1/1") != -1) {
				p1 = 1;
				newP1state = true;
			}
			else if (req.indexOf("/p1/0") != -1) {
				p1 = 0;
				newP1state = true;
			}
			else if (req.indexOf("/p2/1") != -1) {
				p2 = 1;
				newP2state = true;
			}
			else if (req.indexOf("/p2/0") != -1) {
				p2 = 0;
				newP2state = true;
			}
			else if (req.indexOf("/status/p1") != -1) {
				jsonResponse(client, "p1", p1);
				client.stop();
				return;
			}
			else if (req.indexOf("/status/p2") != -1) {
				jsonResponse(client, "p2", p2);
				client.stop();
				return;
			}
			else if (req.indexOf("/favicon.ico") != -1) {
				client.stop();
				return;
			}
			else {
				Serial.println("Invalid request");
				client.stop();
				return;
			}

			okResponse(client);
			delay(20);
			//stopping client
			client.stop();
		}
	}
}

void loop() {
	processPrevRequest();
	processNextRequest();
}
