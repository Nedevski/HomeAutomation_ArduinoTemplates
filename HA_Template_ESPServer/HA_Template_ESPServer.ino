#include <ESP8266WiFi.h>

const char* ssid = "It-hurts-when-IP";
const char* password = "freeinternet";

WiFiServer server(80);

int pin0 = 16;
int pin1 = 5;

bool newP1state = false;
bool newP2state = false;

int p1;
int p2;

void setup() {
	pinMode(pin0, OUTPUT);
	pinMode(pin1, OUTPUT);

	Serial.begin(115200);
	delay(10);

	// Connect to WiFi network
	Serial.println();
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(ssid);

	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println("");
	Serial.println("WiFi connected");

	// Start the server
	server.begin();
	Serial.println("Server started");

	// Print the IP address
	Serial.println(WiFi.localIP());
}

void okResponse(WiFiClient client) {
	client.println("HTTP/1.1 200 OK");
	client.println("Content-Type: application/json");
	client.println();
	client.println("{\"status\":\"Ok\"}");
}

void badRequestResponse(WiFiClient client) {
	client.println("HTTP/1.1 400 Bad Request");
	client.println("Content-Type: application/json");
	client.println();
	client.println("{\"status\":\"Bad request\"}");
}

void notFoundResponse(WiFiClient client) {
	client.println("HTTP/1.1 404 Not Found");
	client.println("Content-Type: application/json");
	client.println();
}

void jsonResponse(WiFiClient client, String paramName, int val) {
	String json = "{\"param\":\"";
	json += paramName;
	json += "\",\"value\":\"";
	json += val;
	json += "\"}";

	client.println("HTTP/1.1 200 OK");
	client.println("Access-Control-Allow-Origin: *");
	client.println("Content-Type: application/json");
	client.println();
	client.println(json);
}

void processPrevRequest() {
	if (newP1state) {
		if (p1 == 1) {
			digitalWrite(pin0, HIGH);
		}
		else if (p1 == 0) {
			digitalWrite(pin0, LOW);
		}

		newP1state = false;
	}
	else if (newP2state) {
		if (p2 == 1) {
			digitalWrite(pin1, HIGH);
		}
		else if (p2 == 0) {
			digitalWrite(pin1, LOW);
		}

		newP2state = false;
	}
}

void processNextRequest() {
	// Check if a client has connected
	WiFiClient client = server.available();

	if (!client) {
		return;
	}

	// Wait until the client sends some data
	//Serial.println("New client!");
	while (!client.available()) {
		delay(1);
	}

	// Read the first line of the request
	String req = client.readStringUntil('\r');
	//Serial.println(req);

	client.flush();

	if (req.indexOf("/p1/1") != -1) {
		p1 = 1;
		newP1state = true;

		jsonResponse(client, "p1", p1);
		client.stop();
		return;
	}
	else if (req.indexOf("/p1/0") != -1) {
		p1 = 0;
		newP1state = true;

		jsonResponse(client, "p1", p1);
		client.stop();
		return;
	}
	else if (req.indexOf("/p2/1") != -1) {
		p2 = 1;
		newP2state = true;

		jsonResponse(client, "p2", p2);
		client.stop();
		return;
	}
	else if (req.indexOf("/p2/0") != -1) {
		p2 = 0;
		newP2state = true;

		jsonResponse(client, "p2", p2);
		client.stop();
		return;
	}
	else if (req.indexOf("/status/p1") != -1) {
		jsonResponse(client, "p1", p1);
		client.stop();
		return;
	}
	else if (req.indexOf("/status/p2") != -1) {
		jsonResponse(client, "p2", p2);
		client.stop();
		return;
	}
	else if (req.indexOf("/favicon.ico") != -1) {
		client.stop();
		return;
	}
	else {
		Serial.println("Invalid request");
		badRequestResponse(client);
		delay(5);
		client.stop();
		return;
	}

	client.flush();

	// Prepare the response
	okResponse(client);

	// Send the response to the client
	delay(5);
	client.stop();
	Serial.println("Client disonnected");
}

void loop() {
	processPrevRequest();
	processNextRequest();
}
